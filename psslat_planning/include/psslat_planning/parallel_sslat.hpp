
#include <boost/config.hpp>

#include <math.h>
#include <algorithm>
#include <vector>
#include <utility>
#include <iostream>
#include <queue>
#include <boost/graph/visitors.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/breadth_first_search.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/graph/graph_utility.hpp>
#include <psslat_planning/delaunator.hpp>
#include <stdio.h>
#include <cuda.h>
#include <cuda_runtime.h>

namespace PSSLat
{
  extern std::vector<double> current_pos;
  extern double R;
  extern int n_levels;
  extern int n_trunks;
  extern int n_branches;
  extern double collision_radius;
  extern std::string vf_name;
  extern std::map<std::string, std::function<std::pair<double, double>(double, double)>> funcMap;

  struct Vertex
  {
    std::vector<double> state = {0, 0};
    size_t parent_idx = 0;
    double cost_from_parent = 0.0;
    double cost_from_start = 0.0;
    std::vector<size_t> children_set;
    std::vector<size_t> traj_from_parent;
    std::vector<size_t> traj_from_start{0};
  };

  struct Edge
  {
    std::vector<size_t> triangles;
  };

  typedef boost::adjacency_list<
      boost::vecS,
      boost::vecS,
      boost::directedS,
      Vertex, Edge>
      SSLatGraph;

  template <typename T>
  std::vector<std::vector<T>> getUniqueRowsTol(std::vector<std::vector<T>> input, double tol);

  void generateGraph(SSLatGraph &G, const int level);
  void treeLattice(SSLatGraph &G, const int level, const size_t head_descriptor, int idx);
  std::pair<std::vector<double>, std::vector<size_t>> triangulateGraph(const SSLatGraph &G);
  void setEdgeToTriangles(SSLatGraph &G, const std::pair<std::vector<double>, std::vector<size_t>> &DT);
  void printGraph(SSLatGraph &G, bool printE, bool printV);

  void bfsCalculateCosts(SSLatGraph &G, const std::vector<bool> &collision_triangles, const double &x, const double &y);
  double vectorFieldCost(const SSLatGraph &G, const size_t v1_descriptor, const size_t v2_descriptor, const double &x, const double &y);
  inline double phi(const double &x1, const double &x2);
  inline double phi_rectangular(const double &x1, const double &x2);
  std::pair<double, double> squareVectorField(const double &x, const double &y);
  std::pair<double, double> spineVectorField(const double &x, const double &y);
  std::pair<double, double> rectangularVectorField(const double &x, const double &y);
  std::pair<double, double> constantVectorField(const double &x, const double &y);
  std::pair<double, double> icraBarnVectorField(const double &x, const double &y);

  std::vector<bool> checkTriangles(const std::vector<float> &ranges, const std::vector<float> &angles, const std::pair<std::vector<double>, std::vector<size_t>> &DT);
  bool pointInTriangle(const std::vector<float> &pt, const std::vector<float> &v1, const std::vector<float> &v2, const std::vector<float> &v3);
  bool triangleInCircle(const std::vector<float> &pt, const std::vector<float> &v1, const std::vector<float> &v2, const std::vector<float> &v3, const float &radius);

  // __global__ void triangleInCircle(float *xrng, float *yrng, float *x1, float *y1, float *x2, float *y2, float *x3, float *y3, float *coltri, int numRanges, int numTriangles);

  inline float sign(const std::vector<float> &p1, const std::vector<float> &p2, const std::vector<float> &p3);

  std::vector<size_t> plan(const SSLatGraph &G);


} // namespace PSSLat