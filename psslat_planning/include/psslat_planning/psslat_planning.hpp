/*!
 * \sslat_planning.h
 * \brief sslat_planning: Sensor Space Lattice Planning Node
 *
 * Sensor Space Lattice Planning Node
 *
 * \author Bernardo Martinez Rocamora Junior, WVU - bm00002@mix.wvu.edu
 * 
 * \date July 21, 2020
 */

#ifndef SSLAT_PLANNING_H
#define SSLAT_PLANNING_H

// Include cpp important headers
#include <math.h>
#include <stdio.h>
#include <chrono>
#include <thread>
#include <termios.h>
#include <vector>
#include <numeric>
#include <iostream>
#include <fstream>

// ROS headers
#include <ros/ros.h>
#include <tf/tf.h>

#include <realtime_tools/realtime_buffer.h>
#include <realtime_tools/realtime_publisher.h>

// Custom message includes. Auto-generated from msg/ directory.
#include <std_msgs/Float64.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Path.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/LaserScan.h>

//
// #include <sslat_planning/sslat.hpp>
#include <psslat_planning/parallel_sslat.hpp>
#include <psslat_planning/delaunator.hpp>


class PSSLatPlanning
{
public:
  PSSLatPlanning(ros::NodeHandle &nh);

  void setupSSLatGraph();

private:
  // Node Handle
  ros::NodeHandle &nh_;

  // Subscriber
  ros::Subscriber sub_odometry_;
  ros::Subscriber sub_laser_scan_;

  // Publisher
  ros::Publisher pub_body_path_;
  ros::Publisher pub_global_path_;

  // Callback functions
  void odometryCallback(const nav_msgs::Odometry::ConstPtr &msg);
  void laserScanCallback(const sensor_msgs::LaserScan::ConstPtr &msg);

  // Precomputed variables
  PSSLat::SSLatGraph G_;
  std::pair<std::vector<double>, std::vector<size_t>> DT_;

  geometry_msgs::Pose current_pose_;
  double current_roll_, current_pitch_, current_yaw_;
  std::string body_frame;
  std::string odometry_frame;
  std::string odometry_topic_name;
  std::string scan_topic_name;
  std::string body_path_topic_name;
  std::string world_path_topic_name;

  double lidar_min_radius_ = 0.5;
  double height_ = 0.5;
  double lidar_orientation_ = 1;

  void LoadParameters();

  std::vector<double> check_time;
  double mean_check_time = 0.0;
  double stddev_check_time = 0.0;

  std::vector<double> costs_time;
  double mean_costs_time = 0.0;
  double stddev_costs_time = 0.0;

  std::vector<double> planning_time;
  double mean_planning_time = 0.0;
  double stddev_planning_time = 0.0;

  int iter_counter = 0;
};

#endif // SSLAT_PLANNING_H