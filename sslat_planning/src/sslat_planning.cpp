/*!
 * \sslat_planning.cpp
 * \brief sslat_planning: Sensor Space Lattice Planning Node
 *
 * Sensor Space Lattice Planning Node
 * 
 * \author Bernardo Martinez Rocamora Junior, WVU - bm00002@mix.wvu.edu
 * 
 * \date May 21, 2022
 */

#include <sslat_planning/sslat_planning.hpp>

SSLatPlanning::SSLatPlanning(ros::NodeHandle &nh)
    : nh_(nh)
{
  LoadParameters();
  
  // Subscriber
  sub_odometry_ = nh_.subscribe(odometry_topic_name, 1, &SSLatPlanning::odometryCallback, this);
  sub_laser_scan_ = nh_.subscribe(scan_topic_name, 1, &SSLatPlanning::laserScanCallback, this);

  // Publisher
  pub_body_path_ = nh_.advertise<nav_msgs::Path>(body_path_topic_name, 1);
  pub_global_path_ = nh_.advertise<nav_msgs::Path>(world_path_topic_name, 1);
}

void SSLatPlanning::LoadParameters()
{
  if (ros::param::get("sslat_planning/body_frame", body_frame) == false)
  {
    ROS_FATAL("No parameter 'body_frame' specified");
    ros::shutdown();
    exit(1);
  }

  if (ros::param::get("sslat_planning/odometry_frame", odometry_frame) == false)
  {
    ROS_FATAL("No parameter 'odometry_frame' specified");
    ros::shutdown();
    exit(1);
  }

  if (ros::param::get("sslat_planning/odometry_topic_name", odometry_topic_name) == false)
  {
    ROS_FATAL("No parameter 'odometry_topic_name' specified");
    ros::shutdown();
    exit(1);
  }
  
  if (ros::param::get("sslat_planning/scan_topic_name", scan_topic_name) == false)
  {
    ROS_FATAL("No parameter 'scan_topic_name' specified");
    ros::shutdown();
    exit(1);
  }

  if (ros::param::get("sslat_planning/body_path_topic_name", body_path_topic_name) == false)
  {
    ROS_FATAL("No parameter 'body_path_topic_name' specified");
    ros::shutdown();
    exit(1);
  }

  if (ros::param::get("sslat_planning/global_path_topic_name", world_path_topic_name) == false)
  {
    ROS_FATAL("No parameter 'world_path_topic_name' specified");
    ros::shutdown();
    exit(1);
  }

  if (ros::param::get("sslat_planning/graph_inner_radius", SSLat::R) == false)
  {
    ROS_FATAL("No parameter 'graph_inner_radius' specified");
    ros::shutdown();
    exit(1);
  }

  if (ros::param::get("sslat_planning/n_levels", SSLat::n_levels) == false)
  {
    ROS_FATAL("No parameter 'n_levels' specified");
    ros::shutdown();
    exit(1);
  }

  if (ros::param::get("sslat_planning/n_trunks", SSLat::n_trunks) == false)
  {
    ROS_FATAL("No parameter 'n_trunks' specified");
    ros::shutdown();
    exit(1);
  }

  if (ros::param::get("sslat_planning/n_branches", SSLat::n_branches) == false)
  {
    ROS_FATAL("No parameter 'n_branches' specified");
    ros::shutdown();
    exit(1);
  }

  if (ros::param::get("sslat_planning/lidar_min_radius", lidar_min_radius_) == false)
  {
    ROS_FATAL("No parameter 'lidar_min_radius' specified");
    ros::shutdown();
    exit(1);
  }

  if (ros::param::get("sslat_planning/collision_radius", SSLat::collision_radius) == false)
  {
    ROS_FATAL("No parameter 'collision_radius' specified");
    ros::shutdown();
    exit(1);
  }

  if (ros::param::get("sslat_planning/vector_field_name", SSLat::vf_name) == false) 
  {
    ROS_FATAL("No parameter 'vector_field_name' specified");
    ros::shutdown();
    exit(1);
  }

  if (ros::param::get("sslat_planning/height", height_) == false) 
  {
    ROS_FATAL("No parameter 'vector_field_name' specified");
    ros::shutdown();
    exit(1);
  }

  if (ros::param::get("sslat_planning/lidar_orientation", lidar_orientation_) == false) 
  {
    ROS_FATAL("No parameter 'lidar_orientation' specified");
    ros::shutdown();
    exit(1);
  }
}

void SSLatPlanning::odometryCallback(const nav_msgs::Odometry::ConstPtr &msg)
{
  current_pose_ = msg->pose.pose;

  tf::Quaternion q(
      current_pose_.orientation.x,
      current_pose_.orientation.y,
      current_pose_.orientation.z,
      current_pose_.orientation.w);

  tf::Matrix3x3 m(q);

  m.getRPY(current_roll_, current_pitch_, current_yaw_);
  // ROS_INFO_STREAM_THROTTLE(5,"(r,p,y): (" << current_roll_  << " ,"  << current_pitch_  << " ,"  << current_yaw_  << ").");

}

void SSLatPlanning::laserScanCallback(const sensor_msgs::LaserScan::ConstPtr &msg)
{
  std::vector<float> ranges, angles;
  for (int i = 0; i < msg->ranges.size(); i++)
  {
    if (!(std::isinf(msg->ranges[i]) || (std::isnan(msg->ranges[i])) || msg->ranges[i]<lidar_min_radius_))
    {
      ranges.push_back(msg->ranges[i]);
      angles.push_back(lidar_orientation_ * (msg->angle_min + msg->angle_increment * i) + current_yaw_);
      // ROS_INFO_STREAM("(rng,ang): (" << msg->ranges[i]  << " ,"  << lidar_orientation_*(msg->angle_min + msg->angle_increment * i) + current_yaw_ << ").");
    }
  }

  
  if (abs(msg->angle_min + M_PI) > 0.1)
  { 
    ranges.push_back(SSLat::R * pow(2,SSLat::n_levels-1));
    angles.push_back(-M_PI);
  }
  if (abs(msg->angle_max - M_PI) > 0.1)
  { 
    ranges.push_back(SSLat::R * pow(2,SSLat::n_levels-1));
    angles.push_back(M_PI);
  }

  double sum, sq_sum;
  double time = ros::Time::now().toNSec();
  std::vector<bool> collision_triangles_ = SSLat::checkTriangles(ranges, angles, DT_);
  // ROS_INFO_STREAM_THROTTLE(5,"[SSLAT] Checking for collisions complete.");
  if(iter_counter > 5)
    check_time.push_back((ros::Time::now().toNSec() - time)*1e-6);

  time = ros::Time::now().toNSec();
  SSLat::bfsCalculateCosts(G_, collision_triangles_, current_pose_.position.x, current_pose_.position.y);
  // ROS_INFO_STREAM_THROTTLE(5,"[SSLAT] Cost calculations complete.");
  if(iter_counter > 5)
    costs_time.push_back((ros::Time::now().toNSec() - time)*1e-6);

  time = ros::Time::now().toNSec();
  std::vector<size_t> path_ids = SSLat::plan(G_);
  // ROS_INFO_STREAM_THROTTLE(5,"[SSLAT] Planning complete.");
  if(iter_counter > 5)
    planning_time.push_back((ros::Time::now().toNSec() - time)*1e-6);

  if(iter_counter > 5)
  {
    sum = std::accumulate(check_time.begin(), check_time.end(), 0.0);
    mean_check_time = sum / check_time.size();
    sq_sum = std::inner_product(check_time.begin(), check_time.end(), check_time.begin(), 0.0);
    stddev_check_time = std::sqrt(sq_sum / check_time.size() - mean_check_time * mean_check_time);

    sum = std::accumulate(costs_time.begin(), costs_time.end(), 0.0);
    mean_costs_time = sum / costs_time.size();
    sq_sum = std::inner_product(costs_time.begin(), costs_time.end(), costs_time.begin(), 0.0);
    stddev_costs_time = std::sqrt(sq_sum / costs_time.size() - mean_costs_time * mean_costs_time);

    sum = std::accumulate(planning_time.begin(), planning_time.end(), 0.0);
    mean_planning_time = sum / planning_time.size();
    sq_sum = std::inner_product(planning_time.begin(), planning_time.end(), planning_time.begin(), 0.0);
    stddev_planning_time = std::sqrt(sq_sum / planning_time.size() - mean_planning_time * mean_planning_time);

    ROS_ERROR("SERIAL");
    ROS_WARN("Iterations: %d", iter_counter-5);
    ROS_WARN("Laser beams: %d", (int) ranges.size());
    ROS_WARN("Collision check time mean: %f ms.", mean_check_time);
    ROS_WARN("Collision check time stddev: %f ms.", stddev_check_time);
    ROS_WARN("Costs time mean: %f ms.", mean_costs_time);
    ROS_WARN("Costs time stddev: %f ms.", stddev_costs_time);
    ROS_WARN("Plan time mean: %f ms.", mean_planning_time);
    ROS_WARN("Plan time stddev: %f ms.", stddev_planning_time);
  }

  nav_msgs::Path body_path;
  body_path.header.stamp = ros::Time::now();
  body_path.header.frame_id = body_frame;
  for (auto itr = path_ids.begin(); itr != path_ids.end(); ++itr)
  {
    geometry_msgs::PoseStamped pose;
    pose.header.stamp = ros::Time::now();
    pose.header.frame_id = body_frame;
    pose.pose.position.x = G_[*itr].state[0] * cos(-current_yaw_) - G_[*itr].state[1] * sin(-current_yaw_);
    pose.pose.position.y = G_[*itr].state[0] * sin(-current_yaw_) + G_[*itr].state[1] * cos(-current_yaw_);
    pose.pose.position.z = height_;
    pose.pose.orientation.w = 1;
    body_path.poses.push_back(pose);
  }
  if (!body_path.poses.empty())
  {
    pub_body_path_.publish(body_path);
  }

  nav_msgs::Path global_path;
  global_path.header.stamp = ros::Time::now();
  global_path.header.frame_id = odometry_frame;
  for (auto itr = path_ids.begin(); itr != path_ids.end(); ++itr)
  {  
    geometry_msgs::PoseStamped pose;
    pose.header.stamp = ros::Time::now();
    pose.header.frame_id = odometry_frame;
    pose.pose.position.x = G_[*itr].state[0] + current_pose_.position.x;
    pose.pose.position.y = G_[*itr].state[1] + current_pose_.position.y;
    pose.pose.position.z = height_;
    pose.pose.orientation.w = 1;
    global_path.poses.push_back(pose);
  }
  if (!global_path.poses.empty())
  {
    pub_global_path_.publish(global_path);
  }
  iter_counter++;
}

void SSLatPlanning::setupSSLatGraph()
{
  SSLat::generateGraph(G_, SSLat::n_levels);
  SSLat::printGraph(G_, 0, 0);

  DT_ = SSLat::triangulateGraph(G_);
  ROS_INFO_STREAM("[SSLAT] Delauney Triangulation complete.");

  // ROS_WARN_STREAM("# Triangles: " << DT_.second.size()/3);
  // for (std::size_t j = 0; j < DT_.second.size(); j += 3)
  // {
  //   ROS_INFO_STREAM(j<< "-th Triangle points: " << "(" << DT_.first[2 * DT_.second[j]] << "," << DT_.first[2 * DT_.second[j] + 1] << "), (" <<
  //     DT_.first[2 * DT_.second[j + 1]] << "," << DT_.first[2 * DT_.second[j + 1] + 1] << "), (" <<
  //     DT_.first[2 * DT_.second[j + 2]] << "," << DT_.first[2 * DT_.second[j + 2] + 1] << ")");
  // }

  SSLat::setEdgeToTriangles(G_, DT_);
  ROS_INFO_STREAM("[SSLAT] Graph generation complete.");
}

/*!
 * \brief Creates and runs the sslat_planning node.
 *
 * \param argc argument count that is passed to ros::init
 * \param argv arguments that are passed to ros::init
 * \return EXIT_SUCCESS if the node runs correctly
 */

int main(int argc, char **argv)
{
  ros::init(argc, argv, "sslat_planning");
  ros::NodeHandle nh("");
  ros::Rate rate(30);

  SSLatPlanning sslat_planning(nh);

  sslat_planning.setupSSLatGraph();

  ros::spin();

  return 0;
}
