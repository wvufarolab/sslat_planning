#include <sslat_planning/sslat.hpp>
#include <sslat_planning/delaunator.hpp>

namespace SSLat
{
  std::vector<double> origin = {0, 0};
  double R = 0.5;
  int n_levels = 4;
  int n_trunks = 32;
  int n_branches = 3;
  double collision_radius = 0.5;
  std::string vf_name = "constantVectorField";
  std::map<std::string, std::function<std::pair<double, double>(double, double)>> funcMap =
      {{"constantVectorField", constantVectorField},
       {"spineVectorField", spineVectorField},
       {"squareVectorField", squareVectorField},
       {"retangularVectorField", rectangularVectorField},
       {"icraBarnVectorField", icraBarnVectorField}};

  template <typename T>
  std::vector<double> linspace(T start_in, T end_in, int num_in)
  {
    std::vector<double> linspaced;

    double start = static_cast<double>(start_in);
    double end = static_cast<double>(end_in);
    double num = static_cast<double>(num_in);

    if (num == 0)
    {
      return linspaced;
    }
    if (num == 1)
    {
      linspaced.push_back(start);
      return linspaced;
    }

    double delta = (end - start) / (num - 1);

    for (int i = 0; i < num - 1; ++i)
    {
      linspaced.push_back(start + delta * i);
    }

    linspaced.push_back(end); // I want to ensure that start and end
                              // are exactly the same as the input
    return linspaced;
  }

  template <typename T>
  std::vector<std::vector<T>> getUniqueRowsTol(std::vector<std::vector<T>> input, double tol)
  {
    // std::sort(input.begin(), input.end());
    // input.erase(std::unique(input.begin(), input.end()), input.end());
    std::vector<std::vector<T>> temp;
    temp.push_back(input.front());

    for (size_t i = 0; i < input.size(); ++i)
    {
      double min_dist = 100;

      for (size_t j = 0; j < temp.size(); ++j)
      {
        double dx = input[i][0] - temp[j][0];
        double dy = input[i][1] - temp[j][1];

        double dist = dx * dx + dy * dy;
        // std::cout << "dx " << dx << ", dy " << dy <<std::endl;
        if (dist < min_dist)
        {
          // std::cout << "dist " << std::hypot(dx, dy) <<std::endl;
          min_dist = dist;
        }
      }

      if (min_dist > tol)
      {
        temp.push_back({input[i][0], input[i][1]});
      }
    }
    return temp;
  }

  double interpolate(std::vector<double> &xData, std::vector<double> &yData, double x, bool extrapolate)
  {
    int size = xData.size();

    int i = 0;                // find left end of interval for interpolation
    if (x >= xData[size - 2]) // special case: beyond right end
    {
      i = size - 2;
    }
    else
    {
      while (x > xData[i + 1])
        i++;
    }
    double xL = xData[i], yL = yData[i], xR = xData[i + 1], yR = yData[i + 1]; // points on either side (unless beyond ends)
    if (!extrapolate)                                                          // if beyond ends of array and not extrapolating
    {
      if (x < xL)
        yR = yL;
      if (x > xR)
        yL = yR;
    }

    double dydx = (yR - yL) / (xR - xL); // gradient

    return yL + dydx * (x - xL); // linear interpolation
  }

  void generateGraph(SSLatGraph &G, const int level)
  {
    Vertex v_start;

    v_start.state[0] = origin[0];
    v_start.state[1] = origin[1];
    // std::cout << "x: " << origin[0] << ", y: " << origin[1] << std::endl;
    size_t v_start_descriptor = add_vertex(v_start, G);

    int lower_level = level - 1;

    for (int i = 0; i < n_trunks; i++)
    {
      treeLattice(G, lower_level, v_start_descriptor, i + 1);
    }
  }

  void treeLattice(SSLatGraph &G, const int level, const size_t head_descriptor, const int idx)
  {
    double r = R * pow(2, (n_levels - level - 1));

    double theta, theta_head;

    if (head_descriptor != 0)
    {
      theta_head = atan2(G[head_descriptor].state[1] - origin[1], G[head_descriptor].state[0] - origin[0]);
      // Bernie Lattice
      theta = theta_head + (2 * M_PI / n_trunks) / (pow((n_branches - 1), (n_levels - level - 1))) * (idx - (n_branches - 1));
      // Bethe Lattice
      // theta = theta_head + (2*pi/(nTrunks*nBranches^(nLevels-1-level))) * (nBranches/2 - length(S.container(head.idx).children_set) - 0.5);
    }
    else
    {
      theta = (2 * M_PI / n_trunks) * (idx - 1);
    }

    Vertex v_new;
    v_new.state[0] = origin[0] + r * cos(theta);
    v_new.state[1] = origin[1] + r * sin(theta);
    size_t v_new_descriptor = add_vertex(v_new, G);

    // if (head_descriptor != v_new_descriptor)
    // {
    // Create edge from head to v_new
    auto e = add_edge(head_descriptor, v_new_descriptor, G).first;
    // Add v_new to the list of neighbors of each element of head
    G[head_descriptor].children_set.push_back(v_new_descriptor);
    // }

    if (level == 0)
    {
      return;
    }

    int lower_level = level - 1;
    for (int i = 0; i < n_branches; i++)
    {
      treeLattice(G, lower_level, v_new_descriptor, i + 1);
    }
  }

  void bfsCalculateCosts(SSLatGraph &G, const std::vector<bool> &collision_triangles, const double &x, const double &y)
  {
    auto itr = vertices(G).first;

    // start bfs at u
    bool *visited = new bool[G.vertex_set().size()];

    for (int i = 0; i < G.vertex_set().size(); i++)
    {
      visited[i] = false;
    }

    std::queue<size_t> active;

    size_t start_descriptor = *itr;

    visited[start_descriptor] = true;
    active.push(start_descriptor);

    while (!active.empty())
    {
      // Dequeue a vertex from queue and print it
      size_t head_descriptor = active.front();
      // std::cout << "Head descriptor: " << head_descriptor << std::endl;

      active.pop();

      // Get all adjacent vertices of the dequeued
      // vertex s. If a adjacent has not been visited,
      // then mark it visited and enqueue it
      for (auto i = G[head_descriptor].children_set.begin(); i != G[head_descriptor].children_set.end(); ++i)
      {
        size_t child_descriptor = *i;
        if (!visited[child_descriptor])
        {
          visited[child_descriptor] = true;
          active.push(child_descriptor);

          double cost_from_parent = vectorFieldCost(G, head_descriptor, child_descriptor, x, y);

          auto epair = out_edges(head_descriptor, G);

          std::vector<size_t> triangles;
          for (auto iter = epair.first; iter != epair.second; iter++)
          {
            size_t child_descriptor_tmp = target(*iter, G);
            if (child_descriptor_tmp == child_descriptor)
            {
              // for (size_t t = 0; t < G[*iter].triangles.size(); ++t)
              // {
              //   std::cout<< "edge: " << *iter << ", triangles: "<< G[*iter].triangles[t] << std::endl;
              // }
              triangles = G[*iter].triangles;
            }
          }

          bool collision = false;
          for (auto t = triangles.begin(); t != triangles.end(); ++t)
          // for (size_t t = 0; t < triangles.size(); ++t)
          {
            // std::cout<< "triangle: " << t << ", collision: "<< collision_triangles[t] << std::endl;
            collision = collision || collision_triangles[*t];
          }

          if (collision)
          {
            // std::cout << "edge with high cost (" << head_descriptor << ","<< child_descriptor << ")" << std::endl;
            cost_from_parent = 10000;
          }

          G[child_descriptor].parent_idx = head_descriptor;
          G[child_descriptor].cost_from_parent = cost_from_parent;
          G[child_descriptor].cost_from_start = G[head_descriptor].cost_from_start + cost_from_parent;
          G[child_descriptor].traj_from_parent.push_back(child_descriptor);
          G[child_descriptor].traj_from_start = G[head_descriptor].traj_from_start;
          G[child_descriptor].traj_from_start.push_back(child_descriptor);
          // std::cout << "(" << G[child_descriptor].state[0] << "," << G[child_descriptor].state[1]<< ")" << std::endl;
          // std::cout << "node: " << child_descriptor
          //           << ", parent: " << head_descriptor
          //           << ", cost from parent: " << cost_from_parent
          //           << ", cost from start: " << G[child_descriptor].cost_from_start << std::endl;
        }
      }
    }
    // for (int i = 0; i<G.vertex_set().size(); i++)
    // {
    //     std::cout << visited[i] << std::endl;
    // }
  }

  void printGraph(SSLatGraph &G, bool printE, bool printV)
  {
    if (printV)
    {
      auto vpair = vertices(G);
      std::cout << "num vertices = " << G.vertex_set().size() << std::endl;
      for (auto iter = vpair.first; iter != vpair.second; iter++)
      {
        std::cout << "vertex " << *iter << ", state = (" << G[*iter].state[0] << "," << G[*iter].state[1] << ")." << std::endl;
        std::cout << "parent " << G[*iter].parent_idx << ", cost = " << G[*iter].cost_from_start << "." << std::endl;
      }
    }

    if (printE)
    {
      auto epair = edges(G);
      for (auto iter = epair.first; iter != epair.second; iter++)
      {
        std::cout << "edge " << (*iter) << ": " << source(*iter, G) << " - " << target(*iter, G) << std::endl;
      }
    }
  }

  std::vector<size_t> plan(const SSLatGraph &G)
  {
    auto vpair = vertices(G);
    // std::cout << "num vertices = " << G.vertex_set().size() << std::endl;
    std::vector<size_t> id_list;
    std::vector<double> cost_list;
    std::vector<size_t> path;

    for (auto iter = vpair.first; iter != vpair.second; iter++)
    {
      if (G[*iter].children_set.empty())
      {
        id_list.push_back(*iter);
        cost_list.push_back(G[*iter].cost_from_start);
      }
    }
    // std::cout << "# outer layer nodes= " << id_list.size() << std::endl;

    bool found_solution = false;
    double min_cost = 100;
    size_t min_cost_id;
    for (size_t i = 0; i < id_list.size(); ++i)
    {
      if (cost_list[i] < min_cost)
      {
        min_cost = cost_list[i];
        min_cost_id = id_list[i];
        found_solution = true;
      }
      // std::cout << "id = " << id_list[i] << ", cost_from_start= " << cost_list[i] << std::endl;
    }

    if (!found_solution)
    {
      // std::cout << "Planner did not find a solution." << std::endl;
      path = G[0].traj_from_start;
    }
    else
    {
      path = G[min_cost_id].traj_from_start;
    }

    // std::cout << "Path with minimum cost of :" <<  min_cost << std::endl;
    // for (auto i = path.begin(); i != path.end(); ++i)
    // {
    //   std::cout << "WP= " << *i << ", ";
    // }
    // std::cout << std::endl;

    return path;
  }

  std::pair<std::vector<double>, std::vector<size_t>> triangulateGraph(const SSLatGraph &G)
  {
    std::vector<std::vector<double>> points;
    std::vector<double> coords;

    auto vpair = vertices(G);

    for (auto iter = vpair.first; iter != vpair.second; iter++)
    {
      points.push_back({G[*iter].state[0], G[*iter].state[1]});
    }

    points = getUniqueRowsTol(points, 0.001);
    // std::cout << "points unique = " << points.size() << std::endl;

    for (size_t i = 0; i < points.size(); ++i)
    {
      coords.push_back(points[i][0]);
      coords.push_back(points[i][1]);
    }

    delaunator::Delaunator d(coords);

    // std::cout << "num triangles = " << d.triangles.size() / 3 << std::endl;

    // for (size_t j = 0; j < d.triangles.size(); j += 3)
    // {
    //   printf(
    //       "Triangle points: [[%f, %f], [%f, %f], [%f, %f]]\n",
    //       d.coords[2 * d.triangles[j]],        //tx0
    //       d.coords[2 * d.triangles[j] + 1],    //ty0
    //       d.coords[2 * d.triangles[j + 1]],    //tx1
    //       d.coords[2 * d.triangles[j + 1] + 1],//ty1
    //       d.coords[2 * d.triangles[j + 2]],    //tx2
    //       d.coords[2 * d.triangles[j + 2] + 1] //ty2
    //   );
    // }

    return std::make_pair(d.coords, d.triangles);
  }

  void setEdgeToTriangles(SSLatGraph &G, const std::pair<std::vector<double>, std::vector<size_t>> &DT)
  {

    std::vector<size_t> edgeToDT;

    double error = 1e-3;

    auto epair = edges(G);

    for (auto iter = epair.first; iter != epair.second; iter++)
    {
      size_t v1 = source(*iter, G);
      size_t v2 = target(*iter, G);
      double v1_x = G[v1].state[0];
      double v1_y = G[v1].state[1];
      double v2_x = G[v2].state[0];
      double v2_y = G[v2].state[1];

      for (size_t j = 0; j < DT.second.size(); j += 3)
      {
        double p1_x = DT.first[2 * DT.second[j]];         // tx0
        double p1_y = DT.first[2 * DT.second[j] + 1];     // ty0
        double p2_x = DT.first[2 * DT.second[j + 1]];     // tx1
        double p2_y = DT.first[2 * DT.second[j + 1] + 1]; // ty1
        double p3_x = DT.first[2 * DT.second[j + 2]];     // tx2
        double p3_y = DT.first[2 * DT.second[j + 2] + 1]; // ty2

        bool a1 = (hypot(v1_x - p1_x, v1_y - p1_y) < error);
        bool a2 = (hypot(v1_x - p2_x, v1_y - p2_y) < error);
        bool a3 = (hypot(v1_x - p3_x, v1_y - p3_y) < error);

        bool b1 = (hypot(v2_x - p1_x, v2_y - p1_y) < error);
        bool b2 = (hypot(v2_x - p2_x, v2_y - p2_y) < error);
        bool b3 = (hypot(v2_x - p3_x, v2_y - p3_y) < error);

        if ((a1 || a2 || a3) && (b1 || b2 || b3))
        {
          G[*iter].triangles.push_back(j);
          // std::cout<< "edge " << (*iter) << " linked to triangle " << j << std::endl;
        }
      }
    }
  }

  inline float sign(const std::vector<float> &p1, const std::vector<float> &p2, const std::vector<float> &p3)
  {
    return (p1[0] - p3[0]) * (p2[1] - p3[1]) - (p2[0] - p3[0]) * (p1[1] - p3[1]);
  }

  bool pointInTriangle(const std::vector<float> &pt, const std::vector<float> &v1, const std::vector<float> &v2, const std::vector<float> &v3)
  {
    float d1, d2, d3;
    bool has_neg, has_pos;

    d1 = sign(pt, v1, v2);
    d2 = sign(pt, v2, v3);
    d3 = sign(pt, v3, v1);

    has_neg = (d1 < 0) || (d2 < 0) || (d3 < 0);
    has_pos = (d1 > 0) || (d2 > 0) || (d3 > 0);

    // std::cout << "Collision: " << !(has_neg && has_pos) << std::endl;

    return !(has_neg && has_pos);
  }

  bool triangleInCircle(const std::vector<float> &pt, const std::vector<float> &v1, const std::vector<float> &v2, const std::vector<float> &v3, const float &radius)
  {
    //
    // TEST 1: Vertex within circle
    //
    float radiusSqr = radius * radius;

    float c1x = pt[0] - v1[0];
    float c1y = pt[1] - v1[1];
    float c1sqr = c1x * c1x + c1y * c1y - radiusSqr;
    if (c1sqr <= 0)
    {
      return true;
    }

    float c2x = pt[0] - v2[0];
    float c2y = pt[1] - v2[1];
    float c2sqr = c2x * c2x + c2y * c2y - radiusSqr;
    if (c2sqr <= 0)
    {
      return true;
    }

    float c3x = pt[0] - v3[0];
    float c3y = pt[1] - v3[1];
    float c3sqr = c3x * c3x + c3y * c3y - radiusSqr;
    if (c3sqr <= 0)
    {
      return true;
    }

    //
    // TEST 2: Circle centre within triangle
    //

    //
    // Calculate edges
    //

    if (pointInTriangle(pt, v1, v2, v3))
    {
      return true;
    }

    //
    // TEST 3: Circle intersects edge
    //

    float e1x = v2[0] - v1[0];
    float e1y = v2[1] - v1[1];

    float e2x = v3[0] - v2[0];
    float e2y = v3[1] - v2[1];

    float e3x = v1[0] - v3[0];
    float e3y = v1[1] - v3[1];

    // First edge
    float k = c1x * e1x + c1y * e1y;
    float len;
    if (k > 0)
    {
      len = e1x * e1x + e1y * e1y; // squared len

      if (k < len)
      {
        if (c1sqr * len - k * k <= 0)
        {
          return true;
        }
      }
    }

    // Second edge
    k = c2x * e2x + c2y * e2y;

    if (k > 0)
    {
      len = e2x * e2x + e2y * e2y;

      if (k < len)
      {
        if (c2sqr * len - k * k <= 0)
        {
          return true;
        }
      }
    }

    // Third edge
    k = c3x * e3x + c3y * e3y;

    if (k > 0)
    {
      len = e3x * e3x + e3y * e3y;

      if (k < len)
      {
        if (c3sqr * len - k * k <= 0)
        {
          return true;
        }
      }
    }

    // No intersection
    return false;
  }

  std::vector<bool> checkTriangles(const std::vector<float> &ranges, const std::vector<float> &angles, const std::pair<std::vector<double>, std::vector<size_t>> &DT)
  {
    std::vector<bool> collision_triangles(DT.second.size(), false);

    std::vector<float> pt{0, 0};

    std::vector<float> v1{0, 0};
    std::vector<float> v2{0, 0};
    std::vector<float> v3{0, 0};

    bool in_collision = false;

    // std::cout << "Prestoring variables complete " << std::endl;
    for (size_t i = 0; i < ranges.size(); i++)
    {
      pt[0] = ranges[i] * cos(angles[i]);
      pt[1] = ranges[i] * sin(angles[i]);

      // std::cout << "Range x:" << pt[0] << " y:"<<pt[1] << std::endl;
      // std::cout << "Triangles "<< DT.second.size()/3 <<  std::endl;

      for (size_t j = 0; j < DT.second.size(); j += 3)
      {

        v1[0] = DT.first[2 * DT.second[j]];         // tx0
        v1[1] = DT.first[2 * DT.second[j] + 1];     // ty0
        v2[0] = DT.first[2 * DT.second[j + 1]];     // tx1
        v2[1] = DT.first[2 * DT.second[j + 1] + 1]; // ty1
        v3[0] = DT.first[2 * DT.second[j + 2]];     // tx2
        v3[1] = DT.first[2 * DT.second[j + 2] + 1]; // ty2

        // std::cout << "Going to check inside triangle"<< std::endl;
        if (triangleInCircle(pt, v1, v2, v3, collision_radius))
        // if (pointInTriangle(pt, v1, v2, v3))
        {
          collision_triangles[j] = true;
          // std::cout << "triangle " << j << "has collision" << std::endl;
        }
      }
    }
    return collision_triangles;
  }

  inline double phi(const double &x1, const double &x2)
  {
    return (pow((x1 / 50.0), 4) - 1.2 * pow((x1 / 50.0), 2) * pow((x2 / 50.0), 2) + pow((x2 / 50.0), 4) - 1);
  }

  std::pair<double, double> squareVectorField(const double &x, const double &y)
  {
    double phi_value = phi(x, y);

    double dx = 0.1;
    double dy = 0.1;
    double dpdx = (phi(x + dx, y) - phi(x - dx, y)) / (2 * dx);
    double dpdy = (phi(x, y + dy) - phi(x, y - dy)) / (2 * dy);

    double grad_norm = hypot(dpdx, dpdy);
    double g = 1 / grad_norm;
    double h = 1 / grad_norm;
    double G = -2 / M_PI * atan(10 * phi_value);
    double H = 1;

    double u = g * G * dpdx + h * H * (-dpdy); // - signal
    double v = g * G * dpdy + h * H * dpdx;
    std::pair<double, double> f(u, v);

    return f;
  }

  inline double phi_rectangular(const double &x1, const double &x2)
  {
    return (pow(((x1 + 0.75) / 3), 8) - 0.6 * pow(((x1 + 0.75) / 3), 4) * pow(((x2) / 10.0), 4) + pow(((x2) / 10.0), 8) - 1);
  }

  std::pair<double, double> rectangularVectorField(const double &x, const double &y)
  {
    double phi_value = phi_rectangular(x, y);

    double dx = 0.1;
    double dy = 0.1;
    double dpdx = (phi_rectangular(x + dx, y) - phi_rectangular(x - dx, y)) / (2 * dx);
    double dpdy = (phi_rectangular(x, y + dy) - phi_rectangular(x, y - dy)) / (2 * dy);

    double grad_norm = hypot(dpdx, dpdy);
    double g = 1 / grad_norm;
    double h = 1 / grad_norm;
    double G = -2 / M_PI * atan(5 * phi_value);
    double H = 1;

    double u = g * G * dpdx + h * H * (-dpdy); // - signal
    double v = g * G * dpdy + h * H * dpdx;
    std::pair<double, double> f(u, v);

    return f;
  }
  
  std::pair<double, double> spineVectorField(const double &x, const double &y)
  {
    double u = 1;
    double c = 2;
    double v = -atan(y / 2); // - signal
    double a = hypot(u, v);
    u = u / a;
    v = v / a;
    std::pair<double, double> f(u, v);

    return f;
  }

  std::pair<double, double> constantVectorField(const double &x, const double &y)
  {
    double u = 1;
    double v = 0;
    double a = hypot(u, v);
    u = u / a;
    v = v / a;
    std::pair<double, double> f(u, v);

    return f;
  }

  std::pair<double, double> icraBarnVectorField(const double &x, const double &y)
  {
    double u = 0.0;
    double v = 1.0;

    if (y >= 7)
    {
      u = 0.0 - x;
      v = 10.0 - y;
    }

    double a = hypot(u, v);
    u = u / a;
    v = v / a;

    std::pair<double, double> f(u, v);

    return f;
  }

  double vectorFieldCost(const SSLatGraph &G, const size_t v1_descriptor, const size_t v2_descriptor, const double &x, const double &y)
  {
    double dx = G[v2_descriptor].state[0] - G[v1_descriptor].state[0];
    double dy = G[v2_descriptor].state[1] - G[v1_descriptor].state[1];
    double c = hypot(dx, dy);
    int N = 10;
    std::vector<double> x_discretized = linspace(G[v1_descriptor].state[0] + origin[0] + x, G[v2_descriptor].state[0] + origin[0] + x, N + 1);
    std::vector<double> y_discretized = linspace(G[v1_descriptor].state[1] + origin[1] + y, G[v2_descriptor].state[1] + origin[1] + y, N + 1);

    for (int i = 1; i < x_discretized.size(); i++)
    {
      double xm = (x_discretized[i] + x_discretized[i - 1]) / 2;
      double ym = (y_discretized[i] + y_discretized[i - 1]) / 2;
      // std::pair<double, double> f = constantVectorField(xm, ym);
      std::pair<double, double> f = funcMap[vf_name](xm, ym); // spineVectorField(xm, ym);

      double u = f.first;
      double v = f.second;
      double g = hypot(u, v);
      c = c - (u * (x_discretized[i] - x_discretized[i - 1]) + v * (y_discretized[i] - y_discretized[i - 1])) / g;
    }
    return c / 2;
  }

} // namespace SSLat
