#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat May 30 12:27:17 2020

@author: danylo
"""

import numpy as np
import rospy
import matplotlib.pyplot as plt
import matplotlib.patches as pth
from tethered_exploration_main.msg import simple_list
from tethered_exploration_main.msg import map_values
from tethered_exploration_main.msg import map_plot

frontier = []
obstacles = []
all_obstacles = []
free_space = []
got_map = False
got_plot = False
resolution = 0.5
xy = []
curr_map = map_values()
robot_base = []
robot_pos = []
plot_path = False
completed = False
path=[]
entire_path = []
tether = []
true_path = []

def callback_map(data):
	global frontier
	global obstacles
	global all_obstacles
	global free_space
	global got_map
	global resolution
	global xy
	global curr_map
	obstacles = []
	curr_map = data
	got_map = True
	frontier = (np.asarray(data.frontier)).reshape(len(data.frontier)/2,2)
	frontier = list(map(tuple,frontier))
	free_space = (np.asarray(data.free_space)).reshape(len(data.free_space)/2,2)
	free_space = list(map(tuple,free_space))
	all_obstacles = (np.asarray(data.all_obstacles)).reshape(len(data.all_obstacles)/2,2)
	all_obstacles = list(map(tuple,all_obstacles))
	for i in np.arange(len(data.obstacles)):
		temp_obstacle = (np.asarray(data.obstacles[i].obstacle)).reshape(len(data.obstacles[i].obstacle)/2,2)
		temp_obstacle = list(map(tuple,temp_obstacle))
		obstacles.append(temp_obstacle)
	resolution = data.resolution
	xy = data.dim
  
def callback_plot(data):
    global robot_base
    global robot_pos
    global plot_path
    global completed
    global path
    global entire_path
    global tether
    global true_path
    global got_plot
    got_plot = True
    robot_base = data.robot_base
    robot_pos = data.robot_pos
    completed = data.completed
    path = (np.asarray(data.path)).reshape(len(data.path)/2,2)
    path = list(map(tuple,path))
    entire_path = (np.asarray(data.entire_path)).reshape(len(data.entire_path)/2,2)
    entire_path = list(map(tuple,entire_path))
    tether = (np.asarray(data.tether)).reshape(len(data.tether)/2,2)
    tether = list(map(tuple,tether))
    true_path = (np.asarray(data.true_path)).reshape(len(data.true_path)/2,2)
    true_path = list(map(tuple,true_path))
    
rospy.init_node("map_animation", anonymous=True)
rospy.Subscriber("/map_converted", map_values, callback_map)
rospy.Subscriber("/plot_data", map_plot, callback_plot)
rate = rospy.Rate(1)
while (not got_map) & (not got_plot) & (not rospy.is_shutdown()):
	rate.sleep()
animation = rospy.get_param('~animation', default=True)

if animation:
    fig, ax = plt.subplots(1,1)
    #fig.set_size_inches(10,10)
    plt.axis(xy)
    ax.set_facecolor('0.75')
    
    ax.cla()
    print("Robot base is",robot_base)
    plt.plot(robot_base[0],robot_base[1],'^r')
    plt.plot(robot_pos[0],robot_pos[1],'*g')
    ax.set_aspect(aspect='equal')
    ax.set_xticks(np.arange(xy[0],xy[1],2))
    ax.set_xticks(np.arange(xy[0],xy[1],resolution), minor=True)
    ax.set_yticks(np.arange(xy[2],xy[3],2))
    ax.set_yticks(np.arange(xy[2],xy[3],resolution), minor=True)
    ax.grid(which='both',alpha=0.5, linestyle='--')
    for i in np.arange(len(obstacles)):
		for j in np.arange(len(obstacles[i])):
		    ax.add_patch(pth.Rectangle(obstacles[i][j],resolution,resolution,color='k'))
    for i in np.arange(len(free_space)):
		ax.add_patch(pth.Rectangle(free_space[i],resolution,resolution,color='w'))
    for i in np.arange(len(frontier)):
		ax.add_patch(pth.Rectangle(frontier[i],resolution,resolution,color='c'))
    plt.pause(0.5)
    
    while not completed:
        
        ax.cla()
        plt.axis(xy)
        ax.set_facecolor('0.75')
        plt.plot(robot_base[0],robot_base[1],'^r')
        plt.plot(robot_pos[0],robot_pos[1],'*g')
        ax.set_aspect(aspect='equal')
        ax.set_xticks(np.arange(xy[0],xy[1],2))
        ax.set_xticks(np.arange(xy[0],xy[1],resolution), minor=True)
        ax.set_yticks(np.arange(xy[2],xy[3],2))
        ax.set_yticks(np.arange(xy[2],xy[3],resolution), minor=True)
        ax.grid(which='both',alpha=0.5, linestyle='--')
        plt.plot([x for (x, y) in entire_path], [y for (x, y) in entire_path], '-y')
        plt.plot([x for (x, y) in tether], [y for (x, y) in tether], '-m')
        plt.plot([x for (x, y) in true_path], [y for (x, y) in true_path], '--g')
        for i in np.arange(len(obstacles)):
            try: 
                for j in np.arange(len(obstacles[i])):
                    ax.add_patch(pth.Rectangle(obstacles[i][j],resolution,resolution,color='k'))
            except: break
        for i in np.arange(len(free_space)):
            try: ax.add_patch(pth.Rectangle(free_space[i],resolution,resolution,color='w'))
            except: break
        for i in np.arange(len(frontier)):
            try: ax.add_patch(pth.Rectangle(frontier[i],resolution,resolution,color='c'))
            except: break
        for i in np.arange(len(obstacles)):
            ax.text(obstacles[i][0][0]+0.1, obstacles[i][0][1]+0.1, str(i+1), color='w')
        if plot_path:
            plt.plot([x for (x, y) in path], [y for (x, y) in path], '-r')
        plt.pause(0.5)