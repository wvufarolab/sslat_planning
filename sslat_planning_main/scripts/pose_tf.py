#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import rospy
import tf
from nav_msgs.msg import Odometry

rospy.init_node('tf_test', anonymous=True)
pub = rospy.Publisher("odom_true", Odometry, queue_size=3)
rate = rospy.Rate(20)
tl = tf.TransformListener()
while not rospy.is_shutdown():
	tl.waitForTransform("/map", "/base_link", rospy.Time(), rospy.Duration(3.0))
	position, quaternion = tl.lookupTransform("/map", "/base_link", rospy.Time())
	odom = Odometry()
	odom.pose.pose.position.x = position[0]
	odom.pose.pose.position.y = position[1]
	odom.pose.pose.position.z = position[2]
	odom.pose.pose.orientation.x = quaternion[0]
	odom.pose.pose.orientation.y = quaternion[1]
	odom.pose.pose.orientation.z = quaternion[2]
	odom.pose.pose.orientation.w = quaternion[3]
	#except: print("Failed to get transform, using previous data")
	pub.publish(odom)
	rate.sleep()
