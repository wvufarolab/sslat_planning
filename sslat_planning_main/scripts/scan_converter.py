#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun May  3 15:58:13 2020

@author: danylo
"""

import numpy as np
import rospy
from sensor_msgs.msg import LaserScan

world = LaserScan()
got_scan = False
published = False

def callback(data):
    global world
    global got_scan
    got_scan = True
    world = data
    
rospy.init_node("scan_receiver", anonymous=True)
pub_topic = rospy.get_param('~pub_topic', default="scan")
sub_topic = rospy.get_param('~sub_topic', default="world")
rospy.Subscriber("/"+sub_topic, LaserScan, callback)
pub = rospy.Publisher("/"+pub_topic, LaserScan, queue_size=1)
rate = rospy.Rate(10)
while (not got_scan) & (not rospy.is_shutdown()):
	rate.sleep()
    
while not rospy.is_shutdown():
    ranges = list(world.ranges)
    scan = world
    scan.header.frame_id = 'scan'
    scan.range_max = 10000
    for i in np.arange(len(ranges)):
        if ranges[i] == np.inf:
            ranges[i] = 10000
    ranges = tuple(ranges)
    scan.ranges = ranges
    
    pub.publish(scan)
    if not published:
        published = True
        print("Scan publisher is now active.")
    rate.sleep()
