#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 27 13:43:58 2020

@author: danylo
"""

import numpy as np
import rospy
import math
import copy
from nav_msgs.msg import OccupancyGrid
from tethered_exploration_main.msg import simple_list
from tethered_exploration_main.msg import map_values

raw_map = OccupancyGrid()
got_map = False
map_res = 0.5
published = False

def callback(data):
    global raw_map
    global got_map
    got_map = True
    raw_map = data
    
def round_points(core, map_res):
    point = []
    point.append((core[0] + map_res, core[1]))
    point.append((core[0] - map_res, core[1]))
    point.append((core[0], core[1] + map_res))
    point.append((core[0], core[1] - map_res))
    point.append((core[0] + map_res, core[1] + map_res))
    point.append((core[0] + map_res, core[1] - map_res))
    point.append((core[0] - map_res, core[1] + map_res))
    point.append((core[0] - map_res, core[1] - map_res))
    return point

def get_pixel(resolution, point):
    pixel = [0, 0]
    if point[0] > 0:
        num = int(point[0])
        while num <= point[0]:
            pixel[0] = round(num,5)
            num += resolution 
    else:
        num = int(point[0])-1
        while num <= point[0]:
            pixel[0] = round(num,5)
            num += resolution
    if point[1] > 0:
        num = int(point[1])
        while num <= point[1]:
            pixel[1] = round(num,5)
            num += resolution 
    else:
        num = int(point[1])-1
        while num <= point[1]:
            pixel[1] = round(num,5)
            num += resolution 
    pixel = (pixel[0],pixel[1])
    return pixel

def get_map_value(all_map, pixel):
    data = all_map.data
    resolution = round(all_map.info.resolution,3)
    width = all_map.info.width
    #height = all_map.info.height
    origin_x = all_map.info.origin.position.x
    origin_y = all_map.info.origin.position.y
    pos_x = (-origin_x + pixel[0])/resolution
    pos_y = (-origin_y + pixel[1])/resolution
    pos = int(pos_y*width + pos_x)
    #value = data[pos]
    return pos

def obstacle_class(obstacles, pixel, resolution):
    ind = None
    for i in np.arange(len(obstacles)):
        test_pixel_r = (round(pixel[0]+resolution,5), pixel[1])
        test_pixel_l = (round(pixel[0]-resolution,5), pixel[1])
        test_pixel_d = (pixel[0], round(pixel[1]-resolution,5))
        test_pixel_u = (pixel[0], round(pixel[1]+resolution,5))
        test_pixel_ru = (round(pixel[0]+resolution,5), round(pixel[1]+resolution,5))
        test_pixel_lu = (round(pixel[0]-resolution,5), round(pixel[1]+resolution,5))
        test_pixel_rd = (round(pixel[0]+resolution,5), round(pixel[1]-resolution,5))
        test_pixel_ld = (round(pixel[0]-resolution,5), round(pixel[1]-resolution,5))
        if test_pixel_u in obstacles[i] or test_pixel_d in obstacles[i]\
        or test_pixel_l in obstacles[i] or test_pixel_r in obstacles[i]\
        or test_pixel_ru in obstacles[i] or test_pixel_lu in obstacles[i]\
        or test_pixel_rd in obstacles[i] or test_pixel_ld in obstacles[i]:
            if ind is None: ind = []
            if i not in ind: ind.append(i)
    return ind

def obstacle_merge(obstacles, pixel, ind, resolution):
    while len(ind) != 1:
        obstacles[ind[0]] += obstacles[ind[1]]
        obstacles.pop(ind[1])
        ind = obstacle_class(obstacles, pixel, resolution)
    return [ind, obstacles]

def scale_update(xy, pixel, resolution):
    xy[0] = min(xy[0], pixel[0]-resolution*2)
    xy[1] = max(xy[1], pixel[0]+resolution*2)
    xy[2] = min(xy[2], pixel[1]-resolution*2)
    xy[3] = max(xy[3], pixel[1]+resolution*2)
    return xy

def get_distance(from_point, to_point):
    dx = from_point[0] - to_point[0]
    dy = from_point[1] - to_point[1]
    d = math.sqrt(dx ** 2 + dy ** 2)
    #angle = math.atan2(-dy,-dx)
    return d#, angle

rospy.init_node("map_receiver", anonymous=True)
rospy.Subscriber("/map", OccupancyGrid, callback)
pub = rospy.Publisher("/map_converted", map_values, queue_size=1)
rate = rospy.Rate(10)
while (not got_map) & (not rospy.is_shutdown()):
	rate.sleep()
c_space = rospy.get_param('~c_space_size', default=0.2)
c_space_rs = rospy.get_param('~c_space_radial_sampling_rate', default=180)
    
while not rospy.is_shutdown():
    map_data = copy.deepcopy(raw_map.data)
    map_res = round(raw_map.info.resolution,5)
    map_width = round(raw_map.info.width,5)
    map_height = round(raw_map.info.height,5)
    map_origin_x = round(raw_map.info.origin.position.x,5)
    map_origin_y = round(raw_map.info.origin.position.y,5)
    pub_map = map_values()
    obstacles=[]
    all_obstacles = []
    frontier=[]
    free_space = []
    frontier_log = []
    pub_map.resolution = round(map_res,3)
    all_map = copy.deepcopy(raw_map)
    dim = [0.0, 0.0, 0.0, 0.0]

    xy = (map_origin_x, map_origin_y)
    all_map_data=list(all_map.data)
    #print(map_data)
    for i in np.arange(len(map_data)):
        if i%map_width == 0 and i != 0:
            xy = (round(map_origin_x,5), round(map_origin_y + ((i/map_width)*map_res),5))
        if map_data[i] > 0:
            #print("Obstacle found at",i)
            for j in np.arange(c_space_rs):    # c-space creation
                point = [xy[0]+(map_res/2), xy[1]+(map_res/2)]
                ex_point = copy.deepcopy(point)
                d = get_distance(point,ex_point)
                angle = (2*np.pi / c_space_rs) * j
                while d < c_space:
                    d = get_distance(point,ex_point)
                    if d >= c_space:
                        ex_point[0] = point[0] + c_space*np.cos(angle)
                        ex_point[1] = point[1] + c_space*np.sin(angle)
                        breaker = True
                    else:
                        ex_point[0] = ex_point[0] + (map_res/2)*np.cos(angle)
                        ex_point[1] = ex_point[1] + (map_res/2)*np.sin(angle)
                        breaker = False
                    pixel = get_pixel(map_res, ex_point)
                    value = get_map_value(raw_map, pixel)
                    if all_map_data[value] != 100:
                        all_map_data[value] = 100
                        #print("Source",xy,"and changed obstacle",pixel,"with tuple index",value)
                    if breaker: break
        xy = (round(xy[0]+map_res,5), round(xy[1],5))
    
    all_map.data = all_map_data
    pub_map.all_map = all_map
    map_data = tuple(all_map_data)
    
    xy = (map_origin_x, map_origin_y)
    for i in np.arange(len(map_data)):
        if i%map_width == 0 and i != 0:
            xy = (round(map_origin_x,5), round(map_origin_y + ((i/map_width)*map_res),5))
        #print xy
        #raw_input()
        if map_data[i] == -1:       # Unknown cell
            ()
        elif map_data[i] == 0:      # Free cell
            dim = scale_update(dim, xy, map_res)
            pub_map.free_space += xy
            #free_space.append(xy)
            if map_data[i-1] == -1 or map_data[i+1] == -1\
            or map_data[i+int(map_width)] == -1 or map_data[i-int(map_width)] == -1:
                #frontier.append(xy)
                pub_map.frontier += xy
        else:                       # Obstacle cell            
            dim = scale_update(dim, xy, map_res)
            obs_class = obstacle_class(obstacles, xy, map_res)
            all_obstacles.append(xy)
            pub_map.all_obstacles += xy
            if obs_class is None:
                obstacles.append([])
                obstacles[-1].append(xy)
            else:
                if len(obs_class) != 1:
                    [obs_class, obstacles] = obstacle_merge(obstacles, xy, obs_class, map_res)
                if xy not in obstacles[obs_class[0]]:
                    obstacles[obs_class[0]].append(xy)
        xy = (round(xy[0]+map_res,5), round(xy[1],5))
    
    for i in np.arange(len(obstacles)):
        obstacles[i].sort()
	obstacles.sort()
    
    for i in np.arange(len(obstacles)):     # Obstacle conversion for publishing
        temp_obstacle = ()
        for j in np.arange(len(obstacles[i])):
            temp_obstacle += obstacles[i][j]
        pub_map.obstacles.append(simple_list(temp_obstacle))
    """    
    for i in np.arange(len(frontier)):      # Frontier conversion for publishing
        point = round_points(frontier[i], map_res)
        for j in np.arange(len(point)):
            if point[j] in free_space and point[j] not in frontier\
            and point[j] not in frontier_log:
                point2 = round_points(point[j],map_res)
                to_add = True
                for k in np.arange(len(point2)):
                    if point2[k] not in free_space:
                        to_add = False
                        break
                if to_add:
                    frontier_log.append(point[j])
                    pub_map.frontier += point[j]
    """
    pub_map.dim = dim
    pub.publish(pub_map)
    if pub_map.free_space != [] and not published:
        published = True
        print("Map publisher is now active.")
    rate.sleep()
