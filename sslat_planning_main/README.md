# README #

### Setup instructions ###

* Clone this repo into your catkin workspace / src
* Either run the following command to clone the gmapping repo into your catkin workspace / src as well, or use your own SLAM package (some re-configuration may be required)
* * git clone https://danylo_s@bitbucket.org/wvufarolab/tethered_exploration_slam_gmapping.git
* For simulating using a drone, also clone the following repository into your catkin workspace
* * git clone https://danylo_s@bitbucket.org/wvufarolab/tethered_exploration_hector_quadrotor.git
* Run catkin_make in your catkin workspace
* Install pyvisgraph by running "pip install pyvisgraph"

### Operation instructions (land robot) ###

* In one terminal, run "roslaunch tethered_exploration_main full.launch". This will initialize the simulation.
* The "main_script.launch" file contains all the relevant parameters that can be tuned.
* Once you're ready to start the exploration, in another terminal run "roslaunch tethered_exploration_main main_script.launch"

### Operation instructions (drone) ###

* In one terminal, run "roslaunch tethered_exploration_main drone_full.launch". This will initialize the simulation.
* The "main_script_drone.launch" file contains all the relevant parameters that can be tuned.
* Once you're ready to start the exploration, in another terminal run "roslaunch tethered_exploration_main main_script_drone.launch"
