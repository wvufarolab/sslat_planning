# SENSOR SPACE LATTICE MOTION PLANNER #


This repository provides the ROS package contaning the Sensor Space Motion Planner and .


### How do I get set up? ###


Install the dependencies 

```
sudo apt-get install ros-noetic-gmapping*
sudo apt-get install ros-noetic-turtlebot*
sudo apt-get install ros-noetic-move-base*

```

Create a workspace: 

```
#!bash

source /opt/ros/noetic/setup.bash # this is usually not necessary since it is done inside .bashrc 
mkdir -p ~/catkin_ws/src
cd catkin_ws/src
catkin_init_workspace

```

Download the package:


```
#!bash
git clone https://bitbucket.org/wvufarolab/sslat_planning.git
git clone https://bitbucket.org/wvufarolab/sslat_simulation.git

```

Install NVidia CUDA Toolkit 11.2 using this [page](https://developer.nvidia.com/Cuda-downloads?target_os=Linux&target_arch=x86_64&target_distro=Ubuntu&target_version=2004&target_type=deblocal)

Compile:

```
#!bash

cd ..
catkin_make
```

Run an example:

```
#!bash

source devel/setup.bash
roslaunch ------ -------

```