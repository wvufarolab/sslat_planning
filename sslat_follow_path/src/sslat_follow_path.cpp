#include "sslat_follow_path/sslat_follow_path.hpp"

SSLatFollowPath::SSLatFollowPath(ros::NodeHandle &nh)
    : nh_(nh)
{
  if (ros::param::get("sslat_follow_path/odometry_topic_name", odometry_topic_name) ==
      false) {
    ROS_FATAL("No parameter 'odometry_topic_name' specified");
    ros::shutdown();
    exit(1);
  }

  if (ros::param::get("sslat_follow_path/cmd_vel_topic_name", cmd_vel_topic_name) ==
      false) {
    ROS_FATAL("No parameter 'cmd_vel_topic_name' specified");
    ros::shutdown();
    exit(1);
  }

  if (ros::param::get("sslat_follow_path/path_topic_name", path_topic_name) ==
      false) {
    ROS_FATAL("No parameter 'path_topic_name' specified");
    ros::shutdown();
    exit(1);
  }

  if (ros::param::get("sslat_follow_path/max_speed", max_speed_) ==
      false) {
    ROS_FATAL("No parameter 'max_speed' specified");
    ros::shutdown();
    exit(1);
  }
  
  if (ros::param::get("sslat_follow_path/weight_factor", weight_factor_) ==
      false) {
    ROS_FATAL("No parameter 'weight_factor' specified");
    ros::shutdown();
    exit(1);
  }

  if (ros::param::get("sslat_follow_path/distance_threshold", distance_threshold_) ==
      false) {
    ROS_FATAL("No parameter 'distance_threshold' specified");
    ros::shutdown();
    exit(1);
  }

  if (ros::param::get("sslat_follow_path/committed_time", committed_time_) ==
      false) {
    ROS_FATAL("No parameter 'committed_time' specified");
    ros::shutdown();
    exit(1);
  }

  // initialize publishers
  pub_cmd_vel_ = nh_.advertise<geometry_msgs::Twist>(cmd_vel_topic_name, 10);

  // initialize subscribers
  sub_path_ = nh_.subscribe(path_topic_name, 1, &SSLatFollowPath::pathCallback, this);
  sub_odometry_ = nh_.subscribe(odometry_topic_name, 1, &SSLatFollowPath::odometryCallback, this);

  timer_ = ros::Time::now();
}
/*******************************************************************************
* Callback functions
*******************************************************************************/

void SSLatFollowPath::odometryCallback(const nav_msgs::Odometry::ConstPtr &msg)
{
  if (firstOdom_ == false)
  {
    firstOdom_ = true;
  }
  // Update pose
  prev_pose_ = current_pose_;
  current_pose_ = msg->pose.pose;
  // Update twist
  prev_twist_ = current_twist_;
  current_twist_ = msg->twist.twist;
  //Init gazebo ros node

  tf::Quaternion q(
      current_pose_.orientation.x,
      current_pose_.orientation.y,
      current_pose_.orientation.z,
      current_pose_.orientation.w);

  tf::Matrix3x3 m(q);
  prev_roll_ = current_roll_;
  prev_pitch_ = current_pitch_;
  prev_yaw_ = current_yaw_;

  m.getRPY(current_roll_, current_pitch_, current_yaw_);

  geometry_msgs::Twist cmd_vel;

  if (!interpolated_path_.poses.empty())
  {
    pathToVector(cmd_vel, interpolated_path_, max_speed_, weight_factor_, distance_threshold_);
  }

  // if(current_pose_.position.y>8.0)
  // {
  //   max_speed_ = 1.0;
  // }

  // ROS_INFO_STREAM("Commanded vel: " << cmd_vel);
  // ROS_INFO_STREAM("Interpolated path: " << interpolated_path_);
  pub_cmd_vel_.publish(cmd_vel);
}

void SSLatFollowPath::pathCallback(const nav_msgs::Path::ConstPtr &msg)
{
  if (firstPath_ == false)
  {
    firstPath_ = true;
  }
  // ROS_INFO_STREAM("Path: " << *msg);

  if (ros::Time::now() - timer_ > ros::Duration(committed_time_) && msg->poses.size()>1)
  {
    interpolated_path_ = interpolatePath(*msg, 0.1);
    timer_ =ros::Time::now();
  }
}

/*******************************************************************************
* Utils
*******************************************************************************/
nav_msgs::Path SSLatFollowPath::interpolatePath(const nav_msgs::Path &path, double segment_length)
{
  std::vector<double> t, x, y;
  int i = 0;
  for (auto iter = path.poses.begin() + 1; iter < path.poses.end(); ++iter)
  {
    t.push_back(i);
    x.push_back(iter->pose.position.x);
    y.push_back(iter->pose.position.y);
    i++;
  }

  ros::Time time = path.header.stamp;
  std::string frame = path.header.frame_id;

  nav_msgs::Path interpolated_path;
  interpolated_path.header.stamp = time;
  interpolated_path.header.frame_id = frame;

  geometry_msgs::PoseStamped pose;

  for (int i = 0; i < (t.size() - 1); i++)
  {
    int num_segments = (int)round(std::hypot((x[i] - x[i + 1]), (y[i] - y[i + 1])) / segment_length);
    std::vector<double> newt = linspace(t[i], t[i + 1], num_segments);

    pose.header.stamp = time;
    pose.header.frame_id = frame;
    std::vector<double> tData{t[i], t[i + 1]};
    std::vector<double> xData{x[i], x[i + 1]};
    std::vector<double> yData{y[i], y[i + 1]};

    for (int j = 0; j < newt.size() - 1; j++)
    {
      pose.pose.position.x = interp1(tData, xData, newt[j], false);
      pose.pose.position.y = interp1(tData, yData, newt[j], false);
      interpolated_path.poses.push_back(pose);
    }
  }

  return interpolated_path;
}

void SSLatFollowPath::pathToVector(geometry_msgs::Twist &cmd_vel, nav_msgs::Path path, double max_speed, double weight_factor, double distance_threshold)
{
  double min_dist = 100;
  int min_dist_index = 0;

  // std::cout << "Node in path: "<<path.poses.size() << std::endl;
  // Get index and minimum distance from the path
  for (int i = 0; i < path.poses.size(); i++)
  {
    double dx = path.poses[i].pose.position.x - current_pose_.position.x;
    double dy = path.poses[i].pose.position.y - current_pose_.position.y;
    double dist = std::hypot(dx, dy);
    if (dist < min_dist)
    {
      min_dist = dist;
      min_dist_index = i;
      // std::cout << "Minimum distance to path: "<< dist << std::endl;
      // std::cout << "Minimum distance index on path: "<< min_dist_index << std::endl;
    }
  }

  // Convergence component
  std::vector<double> C {0,0};

  C[0] = path.poses[min_dist_index].pose.position.x-current_pose_.position.x;
  C[1]  = path.poses[min_dist_index].pose.position.y-current_pose_.position.y;

  double C_norm = hypot(C[0], C[1]) + 0.00001;
  C[0] = C[0]/C_norm;
  C[1] = C[1]/C_norm;

  // Tangent component
  std::vector<double> T {0,0};
  if (min_dist_index > 0)
  {
      T[0] = path.poses[min_dist_index].pose.position.x-path.poses[min_dist_index-1].pose.position.x;
      T[1] = path.poses[min_dist_index].pose.position.y-path.poses[min_dist_index-1].pose.position.y;
  }
  else
  {
      T[0] = path.poses[min_dist_index+1].pose.position.x-path.poses[min_dist_index].pose.position.x;
      T[1] = path.poses[min_dist_index+1].pose.position.y-path.poses[min_dist_index].pose.position.y;
  }

  double T_norm = hypot(T[0], T[1]) + 0.00001;
  T[0] = T[0]/T_norm;
  T[1] = T[1]/T_norm;

  // Compute the weight value
  double W = (2/M_PI)*atan(weight_factor*sqrt(min_dist));

  // Compute the velocity vector
  std::vector<double> v {0,0};
  v[0] = max_speed*(W*C[0] + sqrt(1-W*W)*T[0]);
  v[1] = max_speed*(W*C[1] + sqrt(1-W*W)*T[1]);

  if ((min_dist_index == path.poses.size()-1) && (min_dist < distance_threshold))
  {
      v[0] = 0;
      v[1] = 0;
  }

  double vx = cos(current_yaw_)*v[0] + sin(current_yaw_)*v[1];
  double wz = -sin(current_yaw_)/distance_threshold*v[0] + cos(current_yaw_)/distance_threshold*v[1];

  if (abs(wz) > 0.7)
  {
    vx = 0.0;
  }

  cmd_vel.linear.x = vx > 0.0? vx : 0.0;
  cmd_vel.angular.z = wz;
}

void SSLatFollowPath::pathToVector2(geometry_msgs::Twist &cmd_vel, const nav_msgs::Path path, double max_speed, double distance_threshold)
{
  int path_index = 1; // which index to follow

  std::vector<double> v;
  v[0] = path.poses[path_index].pose.position.x - current_pose_.position.x;
  v[1] = path.poses[path_index].pose.position.y - current_pose_.position.y;

  double v_norm = hypot(v[0], v[1]) + 0.00001; // aka distance from last point in path
  v[0] = v[0] / v_norm;
  v[1] = v[1] / v_norm;

  double angle = atan2((path.poses[path_index].pose.position.y - current_pose_.position.y),
                       (path.poses[path_index].pose.position.x - current_pose_.position.x));

  double error = atan(current_yaw_ - angle);

  double k = 0.7;

  double wz = -sin(current_yaw_) / k * v[0] + cos(current_yaw_) / k * v[1];

  cmd_vel.angular.z = wz;

  if (abs(wz) < 0.1 && abs(error) > 0.8)
  {
    cmd_vel.angular.z = 0.2;
  }

  if (abs(wz) < 0.5 && abs(error) < 0.5)
  {
    cmd_vel.linear.x = (cos(current_yaw_) * v[0] + sin(current_yaw_) * v[1]) * max_speed;
  }
  else if (abs(wz) < 0.2 && abs(error) < 0.5)
  {
    cmd_vel.linear.x = max_speed / 3;
  }
  else
  {
    cmd_vel.linear.x = 0.0;
  }

  if (v_norm < distance_threshold)
  {
    v[0] = 0.0;
    v[1] = 0.0;
  }
}

std::vector<double> SSLatFollowPath::linspace(double start_in, double end_in, int num_in)
{
  std::vector<double> linspaced;

  double start = static_cast<double>(start_in);
  double end = static_cast<double>(end_in);
  double num = static_cast<double>(num_in);

  if (num == 0)
  {
    return linspaced;
  }
  if (num == 1)
  {
    linspaced.push_back(start);
    return linspaced;
  }

  double delta = (end - start) / (num - 1);

  for (int i = 0; i < num - 1; ++i)
  {
    linspaced.push_back(start + delta * i);
  }

  linspaced.push_back(end); // I want to ensure that start and end
                            // are exactly the same as the input
  return linspaced;
}

double SSLatFollowPath::interp1(std::vector<double> &xData, std::vector<double> &yData, double x, bool extrapolate)
{
  int size = xData.size();

  int i = 0;                // find left end of interval for interpolation
  if (x >= xData[size - 2]) // special case: beyond right end
  {
    i = size - 2;
  }
  else
  {
    while (x > xData[i + 1])
      i++;
  }
  double xL = xData[i], yL = yData[i], xR = xData[i + 1], yR = yData[i + 1]; // points on either side (unless beyond ends)
  if (!extrapolate)                                                          // if beyond ends of array and not extrapolating
  {
    if (x < xL)
      yR = yL;
    if (x > xR)
      yL = yR;
  }

  double dydx = (yR - yL) / (xR - xL); // gradient

  return yL + dydx * (x - xL); // linear interpolation
}

/*******************************************************************************
* Main function
*******************************************************************************/
int main(int argc, char *argv[])
{
  ros::init(argc, argv, "sslat_follow_path");

  ros::NodeHandle nh("");

  SSLatFollowPath sslat_follow_path(nh);

  ros::Rate rate(100);

  ros::spin();

  return 0;
}
