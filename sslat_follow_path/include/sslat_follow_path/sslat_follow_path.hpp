
#ifndef TURTLEBOT3_WAYPOINT_NAV_H_
#define TURTLEBOT3_WAYPOINT_NAV_H_

#include <ros/ros.h>
#include <tf/tf.h>

#include <vector>
#include <math.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
#include <actionlib/client/simple_action_client.h>
#include <eigen3/Eigen/Dense>

class SSLatFollowPath
{
 public:
  SSLatFollowPath(ros::NodeHandle & nh);
  bool controlLoop();
  bool firstOdom_;
  bool firstPath_;

 private:
  // ROS NodeHandle
  ros::NodeHandle & nh_;

  // ROS Parameters

  // ROS Time

  // ROS Topic Publishers
  ros::Publisher pub_cmd_vel_;

  // ROS Topic Subscribers
  ros::Subscriber sub_path_;
  ros::Subscriber sub_odometry_;

  // Variables
  ros::Time timer_;
  geometry_msgs::Pose current_pose_, prev_pose_;
  geometry_msgs::Twist current_twist_, prev_twist_;
  nav_msgs::Path interpolated_path_;
  geometry_msgs::Pose goal_pose_;

  double current_roll_, current_pitch_, current_yaw_;
  double prev_roll_, prev_pitch_, prev_yaw_;

  std::string odometry_topic_name;
  std::string cmd_vel_topic_name;
  std::string path_topic_name;
  double max_speed_;
  double weight_factor_;
  double distance_threshold_;
  double committed_time_;

  // Function prototypes
  void pathCallback(const nav_msgs::Path::ConstPtr &msg);
  void odometryCallback(const nav_msgs::Odometry::ConstPtr &msg);
  nav_msgs::Path interpolatePath(const nav_msgs::Path & path, double segment_length);
  void pathToVector(geometry_msgs::Twist & cmd_vel, nav_msgs::Path path, double max_speed, double weight_factor, double distance_threshold);
  void pathToVector2(geometry_msgs::Twist & cmd_vel, nav_msgs::Path path, double max_speed, double distance_threshold);
  double interp1(std::vector<double> &xData, std::vector<double> &yData, double x, bool extrapolate);
  std::vector<double> linspace(double start_in, double end_in, int num_in);
};
#endif // TURTLEBOT3_WAYPOINT_NAV_H_
